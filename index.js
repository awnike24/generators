const { emailGenerator, emailService} = require('./core/consts');

(async function sendEmail (status) {
    try {
        const email = await emailService.sendMail(emailGenerator(status));
        console.log('Email Sent:' + email);
    } catch (error) {
        console.log(error);
    }
}(0));